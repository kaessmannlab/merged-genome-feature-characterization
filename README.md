# Charactization of gene sequence

This collection of scripts was designed to characterize annotated sequences in a genome.
Since we are interested in the greater picture, specified features (e.g. exons) are merged on a per-gene basis.
Overlapping features are combined, as described in the [bedtools manual page](https://bedtools.readthedocs.io/en/latest/content/tools/merge.html).

# Running the pipeline

**IMPORTANT** This pipeline assumes 20 cores, if you want to run it locally, please adapt the `-P 20` flag in the scripts accordingly.

## What you need
1. genome as FASTA file
2. genome annotation as GTF file
3. genome FASTA index, can be created using `faidx`

Then, using `conda` or `mamba`, you can install the provided conda environment.
This just ensures a python version of `3.9`.
If you have this version on your system, already, you can omit this step.

```bash
mamba env create -f env.yaml
```

## Running

```bash
GTFFILE=genes.gtf  # path to the GTF file
FEATURE=exon  # feature to filter for (see third column in GTF file)
FASTAFILE=genome.fa  # path to genome FASTA file
PREFIX=mouse  #  prefix which is added to all outputs

./run_all.sh $GTFFILE $FEATURE $FASTAFILE $PREFIX
```

This command places a per gene quantification CSV file in the `$PREFIX_$FEATURE_quant_per_gene` folder.
It's structure: `gene_id,length,GC_content`.
Additionally it creates a summary table in `$PREFIX_$FEATURE_quantification.csv`
