#!/bin/bash

GTFFILE=$1
FEATURE=$2
PREFIX=$3

cat ${PREFIX}_gene_ids/${FEATURE}_all_gene_ids.txt |
  xargs -P 15 -n1 -I {} scripts/get_feature_locations.sh {} $FEATURE $GTFFILE $PREFIX

