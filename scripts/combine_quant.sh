#!/bin/bash

export INFOLDER=$1
export OUTFILE=$2

echo $INFOLDER
echo $OUTFILE

find $INFOLDER -type f \
  -exec sh -c 'cat "$@" >> $OUTFILE' sh {} +

