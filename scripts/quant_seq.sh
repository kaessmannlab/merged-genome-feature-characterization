#!/bin/bash


FASTAFILE=$1
GENEID=$2
FEATURE=$3
PREFIX=$4

mkdir -p ${PREFIX}_merged_${FEATURE}_quant_per_gene

BEDFILE="${PREFIX}_merged_${FEATURE}_per_gene/${GENEID}.tsv"

QUANT=$(bedtools getfasta \
  -fi $FASTAFILE \
  -bed $BEDFILE | \
  grep -vE '^>' | \
  sed 's/./&\n/g' | \
  grep -vE "^$" | \
  sort | \
  uniq -ci | \
  sed 's/^ *//g' | \
  scripts/parse_basecount.py)

echo "$GENEID,$QUANT" > ${PREFIX}_merged_${FEATURE}_quant_per_gene/${GENEID}.csv
