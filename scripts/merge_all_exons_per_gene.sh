#!/bin/bash

gtffile=$1
FEATURE=$2

cat all_gene_ids.txt | \
  xargs -n1 -I {} -P 15 ./get_exons.sh {} $FEATURE
