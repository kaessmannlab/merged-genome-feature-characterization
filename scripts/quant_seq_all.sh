#!/bin/bash

cat all_gene_ids.txt |
  grep -e '^ENS' |
  xargs -P 20 -n1 \
  ./quant_seq.sh genome.fa


find ./merged_exons_quant_per_gene -type f \
  -exec sh -c 'cat "$@" >> quantification.csv' sh {} +
