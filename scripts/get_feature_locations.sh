#!/bin/bash

GENEID=$1
FEATURE=$2
GTFFILE=$3
PREFIX=$4

mkdir -p ${PREFIX}_${FEATURE}_per_gene
grep "$GENEID" $GTFFILE | \
  awk -v FEATURE=$FEATURE 'BEGIN{FS="\t"}{if($3 == FEATURE) print $0}' > ${PREFIX}_${FEATURE}_per_gene/$GENEID.gtf

bedtools sort -i ${PREFIX}_${FEATURE}_per_gene/$GENEID.gtf > ${PREFIX}_${FEATURE}_per_gene/${GENEID}_sorted.gtf
bedtools merge -i ${PREFIX}_${FEATURE}_per_gene/${GENEID}_sorted.gtf > ${PREFIX}_${FEATURE}_per_gene/${GENEID}_merged.gtf

mkdir -p ${PREFIX}_merged_${FEATURE}_per_gene
awk 'BEGIN{FS="\t"; OFS=FS}{print $0,$3-$2}' \
  ${PREFIX}_${FEATURE}_per_gene/${GENEID}_merged.gtf > ${PREFIX}_merged_${FEATURE}_per_gene/${GENEID}.tsv

