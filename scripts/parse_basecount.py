#!/usr/bin/env python

import sys

basecounts = {}
all = 0
gc = 0

for line in sys.stdin:
    line = line.rstrip().split(' ')
    n = int(line[0])
    base = line[1]

    basecounts[base] = n
    all += n

if 'G' in basecounts.keys():
    gc += basecounts['G']

if 'C' in basecounts.keys():
    gc += basecounts['C']

gc = round(gc / all, 6)

print(f'{all},{gc}')

