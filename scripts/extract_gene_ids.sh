#!/bin/bash

GTFFILE=$1
FEATURE=$2
PREFIX=$3

mkdir -p ${PREFIX}_gene_ids

grep "$FEATURE" $GTFFILE |
  cut -d ';' -f 1 |
  sed 's/"//g' |
  sed 's/gene_id //g' |
  cut -f 9 |
  sort |
  uniq > ${PREFIX}_gene_ids/${FEATURE}_all_gene_ids.txt

