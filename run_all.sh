#!/bin/bash

GTFFILE=$1
FEATURE=$2
FASTAFILE=$3
PREFIX=$4

echo "EXTRACTING NEEDED GENE IDS"
scripts/extract_gene_ids.sh $GTFFILE $FEATURE $PREFIX

GENEIDFILE="${PREFIX}_gene_ids/${FEATURE}_all_gene_ids.txt"

echo "PROCESSING THE FEATURE LOCATIONS"
scripts/get_all_feature_locations.sh $GTFFILE $FEATURE $PREFIX

echo "QUANTIFICATION"
cat $GENEIDFILE |
  xargs -n1 -I {} -P 15 scripts/quant_seq.sh $FASTAFILE {} $FEATURE $PREFIX

echo "COMBINING RESULTS"
echo "gene_id,length,gc" > ${PREFIX}_${FEATURE}_quantification.csv
scripts/combine_quant.sh ${PREFIX}_merged_${FEATURE}_quant_per_gene ${PREFIX}_${FEATURE}_quantification.csv

